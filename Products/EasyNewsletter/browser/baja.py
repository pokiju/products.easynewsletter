# -*- coding: utf-8 -*-
from Products.Five.browser import BrowserView
from plone.autoform.form import AutoExtensibleForm
from plone.app.z3cform import layout
from zope import interface
from zope import schema
from zope import component
from zope.schema.vocabulary import SimpleVocabulary
from zope.schema.vocabulary import SimpleTerm
from Products.CMFCore.utils import getToolByName
from zope.schema.interfaces import IContextSourceBinder
from five import grok
from z3c.form.browser.checkbox import CheckBoxFieldWidget
from z3c.form import form, field, button
from urllib import urlencode
from AccessControl.SecurityManagement import newSecurityManager
from urlparse import urlsplit
import ast
import re

from Products.EasyNewsletter import EasyNewsletterMessageFactory as _

EMAIL_RE = "^([0-9a-zA-Z_&.+-]+!)*[0-9a-zA-Z_&.+-]+@(([0-9a-zA-Z]([0-9a-zA-Z-]*[0-9a-z-A-Z])?\.)+[a-zA-Z]{2,6}|([0-9]{1,3}\.){3}[0-9]{1,3})$"

@grok.provider(IContextSourceBinder)
def vocab_newsletters(context):
    catalog = getToolByName(context, 'portal_catalog')
    res = catalog.searchResults(portal_type=('EasyNewsletter'))

    newsletters = []
    for item in res:
        newsletters.append((item.id, item.Title))

    return SimpleVocabulary(make_terms(newsletters))

def make_terms(items):
    """ Create zope.schema terms for vocab from tuples """
    terms = [SimpleTerm(value=pair[0], token=pair[0], title=pair[1]) for pair in items]
    return terms


class InvalidEmailError(schema.ValidationError):
    __doc__ = u'Please enter a valid e-mail address.'


def isEmail(value):
    if re.match('^'+EMAIL_RE, value):
        return True
    raise InvalidEmailError


class DelFormSchema(interface.Interface):
    email = schema.TextLine(
        title=_(u"Adreça electrònica"),
        constraint=isEmail,
    )
    newsletters = schema.List(
        title=_(u"Newsletters"),
        value_type=schema.Choice(source=vocab_newsletters),
    )

class DelFormAdapter(object):
    interface.implements(DelFormSchema)
    component.adapts(interface.Interface)
    def __init__(self, context):
        self.email = None
        self.newsletters = None

class DelForm(AutoExtensibleForm, form.Form):
    schema = DelFormSchema
    form_name = 'add_content'
    fields = field.Fields(DelFormSchema)
    fields['newsletters'].widgetFactory = CheckBoxFieldWidget

    @button.buttonAndHandler(u'Eliminar')
    def handleApply(self, action):
        data, errors = self.extractData()

        if errors:
            self.status = self.formErrorsMessage
            return

        catalog = getToolByName(self.context, 'portal_catalog')
        res_boletines = catalog.searchResults(portal_type=('EasyNewsletter'), id=data['newsletters'])
        resultados = {}
        for item in range(len(res_boletines)):
            boletin = res_boletines[item].getObject()
            resultados[boletin.id] = []
            path = boletin.absolute_url_path()
            res_subscriptores = catalog.unrestrictedSearchResults(portal_type=('ENLSubscriber'), path=path)
            for item2 in res_subscriptores:
                resultados[boletin.id].append((item2.email, item2.UID))
        resultado_final = []
        for key in resultados.keys():
            newsletter = catalog.searchResults(portal_type=('EasyNewsletter'), id=key)[0].getObject()
            if data['email'] in [x for (x,y) in resultados[key]]:
                # We do the deletion as the owner of the newsletter object
                # so that this is possible without login.
                owner = newsletter.getWrappedOwner()
                newSecurityManager(newsletter, owner)
                uid = [y for (x,y) in resultados[key] if x==data['email']]
                catalog2 = getToolByName(newsletter, "reference_catalog")
                subscriber = catalog2.lookupObject(uid[0])
                del newsletter[subscriber.id]
                resultado_final.append((True, newsletter.title))
            else:
                resultado_final.append((False, newsletter.title))

        self.request.response.redirect('confirmacio_baixa?'+ urlencode({'resultados': resultado_final}))

    @button.buttonAndHandler(u"Cancelar")
    def handleCancel(self, action):
        """User cancelled. Redirect back to the front page.
        """

class DelButton(layout.FormWrapper):
    """Add button"""
    form = DelForm
    label = _(u"Baixa subscripcions")

class Cancelacion(object):

    def __init__(self, context, request):
        self.context = context
        self.request = request

    def datos(self):
        datos = urlsplit(self.request.resultados).path
        aux = ast.literal_eval(datos)
        print aux
        res = []
        for item in aux:
            if item[0] == True:
                res.append(item[1])
        return res