# -*- coding: utf-8 -*-
from Products.Five.browser import BrowserView
from plone.autoform.form import AutoExtensibleForm
from plone.app.z3cform import layout
from zope import interface
from zope import schema
from zope import component
from zope.schema.vocabulary import SimpleVocabulary
from zope.schema.vocabulary import SimpleTerm
from Products.CMFCore.utils import getToolByName
from zope.schema.interfaces import IContextSourceBinder
from five import grok
from z3c.form.browser.checkbox import CheckBoxFieldWidget
from z3c.form import form, field, button
from urllib import urlencode
from urlparse import urlsplit
from smtplib import SMTPRecipientsRefused
from plone import api

import pdb

import ast
import re

from Products.EasyNewsletter import EasyNewsletterMessageFactory as _

EMAIL_RE = "^([0-9a-zA-Z_&.+-]+!)*[0-9a-zA-Z_&.+-]+@(([0-9a-zA-Z]([0-9a-zA-Z-]*[0-9a-z-A-Z])?\.)+[a-zA-Z]{2,6}|([0-9]{1,3}\.){3}[0-9]{1,3})$"

@grok.provider(IContextSourceBinder)
def vocab_newsletters(context):
    catalog = getToolByName(context, 'portal_catalog')
    res = catalog.searchResults(portal_type=('EasyNewsletter'))

    newsletters = []
    for item in res:
        newsletters.append((item.id, item.Title))

    return SimpleVocabulary(make_terms(newsletters))

def make_terms(items):
    """ Create zope.schema terms for vocab from tuples """
    terms = [SimpleTerm(value=pair[0], token=pair[0], title=pair[1]) for pair in items]
    return terms

sexo_vocab = SimpleVocabulary(make_terms([("hombre", "Hombre"), ("mujer", "Mujer")]))

pais_vocab = SimpleVocabulary(make_terms([
    ("albanian", _(u"Albanian")),
    ("algerian", _(u"Algerian")),
    ("american", _(u"American")),
    ("andorran", _(u"Andorra")),
    ("angolan", _(u"Angolan")),
    ("antiguans", _(u"Antiguans")),
    ("argentinean", _(u"Argentinean")),
    ("armenian", _(u"Armenian")),
    ("australian", _(u"Australian")),
    ("austrian", _(u"Austrian")),
    ("azerbaijani", _(u"Azerbaijani")),
    ("bahamian", _(u"Bahamian")),
    ("bahraini", _(u"Bahraini")),
    ("bangladeshi", _(u"Bangladeshi")),
    ("barbadian", _(u"Barbadian")),
    ("barbudans", _(u"Barbudans")),
    ("batswana", _(u"Batswana")),
    ("belarusian", _(u"Belarusian")),
    ("belgian", _(u"Belgian")),
    ("belizean", _(u"Belizean")),
    ("beninese", _(u"Beninese")),
    ("bhutanese", _(u"Bhutanese")),
    ("bolivian", _(u"Bolivian")),
    ("bosnian", _(u"Bosnian")),
    ("brazilian", _(u"Brazilian")),
    ("british", _(u"British")),
    ("bruneian", _(u"Bruneian")),
    ("bulgarian", _(u"Bulgarian")),
    ("burkinabe", _(u"Burkinabe")),
    ("burmese", _(u"Burmese")),
    ("burundian", _(u"Burundian")),
    ("cambodian", _(u"Cambodian")),
    ("cameroonian", _(u"Cameroonian")),
    ("canadian", _(u"Canadian")),
    ("cape verdean", _(u"Cape Verdean")),
    ("central african", _(u"Central African")),
    ("chadian", _(u"Chadian")),
    ("chilean", _(u"Chilean")),
    ("chinese", _(u"Chinese")),
    ("colombian", _(u"Colombian")),
    ("comoran", _(u"Comoran")),
    ("congolese", _(u"Congolese")),
    ("costa rican", _(u"Costa Rican")),
    ("croatian", _(u"Croatian")),
    ("cuban", _(u"Cuban")),
    ("cypriot", _(u"Cypriot")),
    ("czech", _(u"Czech")),
    ("danish", _(u"Danish")),
    ("djibouti", _(u"Djibouti")),
    ("dominican", _(u"Dominican")),
    ("dutch", _(u"Dutch")),
    ("east timorese", _(u"East Timorese")),
    ("ecuadorean", _(u"Ecuadorean")),
    ("egyptian", _(u"Egyptian")),
    ("emirian", _(u"Emirian")),
    ("equatorial guinean", _(u"Equatorial Guinean")),
    ("eritrean", _(u"Eritrean")),
    ("estonian", _(u"Estonian")),
    ("ethiopian", _(u"Ethiopian")),
    ("fijian", _(u"Fijian")),
    ("filipino", _(u"Filipino")),
    ("finnish", _(u"Finnish")),
    ("french", _(u"French")),
    ("gabonese", _(u"Gabonese")),
    ("gambian", _(u"Gambian")),
    ("georgian", _(u"Georgian")),
    ("german", _(u"German")),
    ("ghanaian", _(u"Ghanaian")),
    ("greek", _(u"Greek")),
    ("grenadian", _(u"Grenadian")),
    ("guatemalan", _(u"Guatemalan")),
    ("guinea-bissauan", _(u"Guinea-Bissauan")),
    ("guinean", _(u"Guinean")),
    ("guyanese", _(u"Guyanese")),
    ("haitian", _(u"Haitian")),
    ("herzegovinian", _(u"Herzegovinian")),
    ("honduran", _(u"Honduran")),
    ("hungarian", _(u"Hungarian")),
    ("icelander", _(u"Icelander")),
    ("indian", _(u"Indian")),
    ("indonesian", _(u"Indonesian")),
    ("iranian", _(u"Iranian")),
    ("iraqi", _(u"Iraqi")),
    ("irish", _(u"Irish")),
    ("israeli", _(u"Israeli")),
    ("italian", _(u"Italian")),
    ("ivorian", _(u"Ivorian")),
    ("jamaican", _(u"Jamaican")),
    ("japanese", _(u"Japanese")),
    ("jordanian", _(u"Jordanian")),
    ("kazakhstani", _(u"Kazakhstani")),
    ("kenyan", _(u"Kenyan")),
    ("kittian and nevisian", _(u"Kittian and Nevisian")),
    ("kuwaiti", _(u"Kuwaiti")),
    ("kyrgyz", _(u"Kyrgyz")),
    ("laotian", _(u"Laotian")),
    ("latvian", _(u"Latvian")),
    ("lebanese", _(u"Lebanese")),
    ("liberian", _(u"Liberian")),
    ("libyan", _(u"Libyan")),
    ("liechtensteiner", _(u"Liechtensteiner")),
    ("lithuanian", _(u"Lithuanian")),
    ("luxembourger", _(u"Luxembourger")),
    ("macedonian", _(u"Macedonian")),
    ("malagasy", _(u"Malagasy")),
    ("malawian", _(u"Malawian")),
    ("malaysian", _(u"Malaysian")),
    ("maldivan", _(u"Maldivan")),
    ("malian", _(u"Malian")),
    ("maltese", _(u"Maltese")),
    ("marshallese", _(u"Marshallese")),
    ("mauritanian", _(u"Mauritanian")),
    ("mauritian", _(u"Mauritian")),
    ("mexican", _(u"Mexican")),
    ("micronesian", _(u"Micronesian")),
    ("moldovan", _(u"Moldovan")),
    ("monacan", _(u"Monacan")),
    ("mongolian", _(u"Mongolian")),
    ("moroccan", _(u"Moroccan")),
    ("mosotho", _(u"Mosotho")),
    ("motswana", _(u"Motswana")),
    ("mozambican", _(u"Mozambican")),
    ("namibian", _(u"Namibian")),
    ("nauruan", _(u"Nauruan")),
    ("nepalese", _(u"Nepalese")),
    ("new zealander", _(u"New Zealander")),
    ("ni-vanuatu", _(u"Ni-Vanuatu")),
    ("nicaraguan", _(u"Nicaraguan")),
    ("nigerien", _(u"Nigerien")),
    ("north korean", _(u"North Korean")),
    ("northern irish", _(u"Northern Irish")),
    ("norwegian", _(u"Norwegian")),
    ("omani", _(u"Omani")),
    ("pakistani", _(u"Pakistani")),
    ("palauan", _(u"Palauan")),
    ("panamanian", _(u"Panamanian")),
    ("papua new guinean", _(u"Papua New Guinean")),
    ("paraguayan", _(u"Paraguayan")),
    ("peruvian", _(u"Peruvian")),
    ("polish", _(u"Polish")),
    ("portuguese", _(u"Portuguese")),
    ("qatari", _(u"Qatari")),
    ("romanian", _(u"Romanian")),
    ("russian", _(u"Russian")),
    ("rwandan", _(u"Rwandan")),
    ("saint lucian", _(u"Saint Lucian")),
    ("salvadoran", _(u"Salvadoran")),
    ("samoan", _(u"Samoan")),
    ("san marinese", _(u"San Marinese")),
    ("sao tomean", _(u"Sao Tomean")),
    ("saudi", _(u"Saudi")),
    ("scottish", _(u"Scottish")),
    ("senegalese", _(u"Senegalese")),
    ("serbian", _(u"Serbian")),
    ("seychellois", _(u"Seychellois")),
    ("sierra leonean", _(u"Sierra Leonean")),
    ("singaporean", _(u"Singaporean")),
    ("slovakian", _(u"Slovakian")),
    ("slovenian", _(u"Slovenian")),
    ("solomon islander", _(u"Solomon Islander")),
    ("somali", _(u"Somali")),
    ("south african", _(u"South African")),
    ("south korean", _(u"South Korean")),
    ("spanish", _(u"Spanish")),
    ("sri lankan", _(u"Sri Lankan")),
    ("sudanese", _(u"Sudanese")),
    ("surinamer", _(u"Surinamer")),
    ("swazi", _(u"Swazi")),
    ("swedish", _(u"Swedish")),
    ("swiss", _(u"Swiss")),
    ("syrian", _(u"Syrian")),
    ("taiwanese", _(u"Taiwanese")),
    ("tajik", _(u"Tajik")),
    ("tanzanian", _(u"Tanzanian")),
    ("thai", _(u"Thai")),
    ("togolese", _(u"Togolese")),
    ("tongan", _(u"Tongan")),
    ("trinidadian or tobagonian", _(u"Trinidadian or Tobagonian")),
    ("tunisian", _(u"Tunisian")),
    ("turkish", _(u"Turkish")),
    ("tuvaluan", _(u"Tuvaluan")),
    ("ugandan", _(u"Ugandan")),
    ("ukrainian", _(u"Ukrainian")),
    ("uruguayan", _(u"Uruguayan")),
    ("uzbekistani", _(u"Uzbekistani")),
    ("venezuelan", _(u"Venezuelan")),
    ("vietnamese", _(u"Vietnamese")),
    ("welsh", _(u"Welsh")),
    ("yemenite", _(u"Yemenite")),
    ("zambian", _(u"Zambian")),
    ("zimbabwean", _(u"Zimbabwean")),
    ("Otro", _(u"Altre")),
    ]))


class InvalidEmailError(schema.ValidationError):
    __doc__ = u'Please enter a valid e-mail address.'


def isEmail(value):
    if re.match('^'+EMAIL_RE, value):
        return True
    raise InvalidEmailError


class AddFormSchema(interface.Interface):
    nombre = schema.TextLine(
        title=_(u"Nom"),
    )
    apellidos = schema.TextLine(
        title=_(u"Cognoms"),
        required=False,
    )
    email = schema.TextLine(
        title=_(u"Adreça electrònica"),
        constraint=isEmail,
    )
    fecha = schema.Date(
        title=_(u"Data naixement"),
        required=False,
    )
    sexo = schema.Choice(
        title=_(u"Sexe"),
        required=False,
        source=sexo_vocab,
    )
    movil = schema.TextLine(
        title=_(u"Telèfon mòbil"),
        required=False,
    )
    pueblo = schema.TextLine(
        title=_(u"Poble"),
        required=False,
    )
    pais = schema.Choice(
        title=_(u"País"),
        required=False,
        source=pais_vocab,
    )
    newsletters = schema.List(
        title=_(u"Newsletters"),
        value_type=schema.Choice(source=vocab_newsletters),
    )

class AddFormAdapter(object):
    interface.implements(AddFormSchema)
    component.adapts(interface.Interface)
    def __init__(self, context):
        self.nombre = None
        self.apellidos = None
        self.email = None
        self.fecha = None
        self.sexo = None
        self.movil = None
        self.pueblo = None
        self.pais = None
        self.newsletters = None

class AddForm(AutoExtensibleForm, form.Form):
    schema = AddFormSchema
    form_name = 'add_content'
    fields = field.Fields(AddFormSchema)
    fields['newsletters'].widgetFactory = CheckBoxFieldWidget

    @button.buttonAndHandler(u'Subscribir')
    def handleApply(self, action):
        data, errors = self.extractData()

        if errors:
            self.status = self.formErrorsMessage
            return

        #print data
        catalog = getToolByName(self.context, 'portal_catalog')
        res = catalog.searchResults(portal_type=('EasyNewsletter'), id=data['newsletters'], sort_on='sortable_title')
        resultados = []
        for item in range(len(res)):
            boletin = res[item].getObject()
            resultados.append((boletin.addSubscriber(
                subscriber=data['email'],
                movil=data['movil'],
                pueblo=data['pueblo'],
                pais=data['pais'],
                nombre=data['nombre'],
                fullname=data['apellidos'],
                organization='',
                sexo=data['sexo'],
                fecha=data['fecha']),boletin.title,data['email']))

        #print resultados

        self.request.response.redirect('confirmacio_subscripcion?'+ urlencode({'resultados': resultados}))

    @button.buttonAndHandler(u"Cancelar")
    def handleCancel(self, action):
        """User cancelled. Redirect back to the front page.
        """

class AddButton(layout.FormWrapper):
    """Add button"""
    form = AddForm
    label = _(u"Subscripció a la newsletter del Comú d’Ordino")


class Confirmacion(object):

    def __init__(self, context, request):
        self.context = context
        self.request = request

    def envio_mail(self, correo, res):
        host = getToolByName(self, 'MailHost')
        portal_url = getToolByName(self, 'portal_url')
        portal = portal_url.getPortalObject()
        sender = portal.getProperty('email_from_address')

        def lista_boletines():
            cadena = u"<ul>"
            for item in res:
                cadena += "<li>%s"%item
                cadena += "</li>"
            cadena += "</ul>"
            return cadena

        # pdb.set_trace()

        lista_bol = lista_boletines()

        mail_template = u"<html>\
                         <head></head>\
                         <body>\
                            <p><strong>Subscripció a la newsletter del Comú d’Ordino</strong></p>\
                            <p>Confirmació al servei de subscripció dels butlletins electrònics\
                            de notícies del Comú d’Ordino. Vostè ha estat donat d’alta a:</p>\
                            %(lista)s<br/>\
                         </body>\
                         </html>" %{'sender': sender, 'correo': correo, 'lista': lista_bol}

        try:
            # The ``immediate`` parameter causes an email to be sent immediately
            # (if any error is raised) rather than sent at the transaction
            # boundary or queued for later delivery. 'lista': lista_bol.encode("utf-8")}
            return host.send(mail_template, mto=correo,\
                mfrom=sender, subject='Subscripció a la newsletter del Comú d’Ordino',\
                msg_type='text/html', charset='utf8',\
                immediate=True)
        except SMTPRecipientsRefused:
            # Don't disclose email address on failure
            raise SMTPRecipientsRefused('Recipient address rejected by server')
        except Exception:
            pass

    def datos(self):
        datos = urlsplit(self.request.resultados).path
        aux = ast.literal_eval(datos)
        res = []
        correo = ''
        for item in aux:
            if item[0][0] == True:
                res.append(item[1])
                correo = item[2]

        if len(correo)>0:
            self.envio_mail(correo, res)
        return res