# -*- coding: utf-8 -*-
from AccessControl import ClassSecurityInfo
from Products.Archetypes import atapi
from Products.EasyNewsletter import EasyNewsletterMessageFactory as _
from Products.EasyNewsletter import config
from Products.EasyNewsletter.interfaces import IENLSubscriber
from zope.interface import implements


schema = atapi.BaseSchema + atapi.Schema((

    atapi.StringField(
        'title',
        required=False,
        widget=atapi.StringWidget(
            visible={'edit': 'invisible', 'view': 'invisible'},
            label=_(u'EasyNewsletter_label_title', default=u'Title'),
            description=_(u'EasyNewsletter_help_title', default=u''),
            i18n_domain='EasyNewsletter',
        ),
    ),

    atapi.StringField(
        'salutation',
        required=False,
        vocabulary=config.SALUTATION,
        widget=atapi.SelectionWidget(
            visible={'edit': 'invisible', 'view': 'invisible'},
            label=_(
                u'EasyNewsletter_label_salutation',
                default='Salutation'),
            description=_('EasyNewsletter_help_salutation', default=u''),
            i18n_domain='EasyNewsletter',
            format='select',
        ),
    ),

    atapi.StringField(
        'nombre',
        required=False,
        widget=atapi.StringWidget(
            label=_(u'EasyNewsletter_label_nombre', default=u'Nombre'),
            description=_('EasyNewsletter_help_nombre', default=u''),
            i18n_domain='EasyNewsletter',
        ),
    ),

    atapi.StringField(
        'fullname',
        required=False,
        widget=atapi.StringWidget(
            label=_(u'EasyNewsletter_label_fullname', default=u'Apellidos'),
            description=_('EasyNewsletter_help_fullname', default=u''),
            i18n_domain='EasyNewsletter',
        ),
    ),

    atapi.StringField(
        'email',
        required=True,
        validators=('isEmail', ),
        widget=atapi.StringWidget(
            label=_(
                u'EasyNewsletter_label_email',
                default=u'Email'),
            description=_(
                u'EasyNewsletter_help_email',
                default=u''),
            i18n_domain='EasyNewsletter',
        ),
    ),

    atapi.DateTimeField(
        'fecha',
        required=False,
        widget=atapi.CalendarWidget(
            label=_(u'EasyNewsletter_label_fecha', default=u'Fecha de nacimiento'),
            description=_('EasyNewsletter_help_fecha', default=u''),
            i18n_domain='EasyNewsletter',
            format='%d-%m-%Y',
            future_years=0,
            starting_year=1940,
            show_hm=False,
        ),
    ),

    atapi.StringField(
        'sexo',
        required=False,
        vocabulary=config.SEXO,
        widget=atapi.SelectionWidget(
            label=_(
                u'EasyNewsletter_label_sexo',
                default='Sexo'),
            description=_('EasyNewsletter_help_sexo', default=u''),
            i18n_domain='EasyNewsletter',
            format='select',
        ),
    ),

    atapi.StringField(
        'movil',
        required=False,
        widget=atapi.StringWidget(
            label=_(u'EasyNewsletter_label_movil', default=u'Movil'),
            description=_('EasyNewsletter_help_movil', default=u''),
            i18n_domain='EasyNewsletter',
        ),
    ),

    atapi.StringField(
        'pueblo',
        required=False,
        widget=atapi.StringWidget(
            label=_(u'EasyNewsletter_label_pueblo', default=u'Pueblo'),
            description=_('EasyNewsletter_help_pueblo', default=u''),
            i18n_domain='EasyNewsletter',
        ),
    ),

    atapi.StringField(
        'pais',
        required=False,
        vocabulary=config.PAISES,
        widget=atapi.SelectionWidget(
            label=_(
                u'EasyNewsletter_label_pais',
                default='Pais'),
            description=_('EasyNewsletter_help_pais', default=u''),
            i18n_domain='EasyNewsletter',
            format='select',
        ),
    ),

    atapi.StringField(
        'organization',
        required=False,
        widget=atapi.StringWidget(
            visible={'edit': 'invisible', 'view': 'invisible'},
            label=_(
                u'EasyNewsletter_label_organization',
                default=u'Company/Organization'),
            description=_(
                'EasyNewsletter_help_organization',
                default=u''),
            i18n_domain='EasyNewsletter',
        ),
    ),

), )


class ENLSubscriber(atapi.BaseContent):
    """An newsletter subscriber.
    """
    implements(IENLSubscriber)
    security = ClassSecurityInfo()
    schema = schema
    _at_rename_after_creation = True

    def initializeArchetype(self, **kwargs):
        """Overwritten hook
        """
        atapi.BaseContent.initializeArchetype(self, **kwargs)

    def setEmail(self, value):
        """
        """
        self.email = value
        self.title = value

        # reindex to set title for catalog
        self.reindexObject()

    def Title(self):
        """Overwritten accessor for Title
        """
        title_str = self.getEmail()
        if self.getFullname():
            title_str += ' - ' + self.getFullname()
        return title_str


atapi.registerType(ENLSubscriber, config.PROJECTNAME)
