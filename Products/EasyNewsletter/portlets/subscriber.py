# -*- coding: utf-8 -*-
from Products.EasyNewsletter import EasyNewsletterMessageFactory as _
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from plone.app.portlets.portlets import base
from plone.app.vocabularies.catalog import SearchableTextSourceBinder
from plone.portlets.interfaces import IPortletDataProvider
from zope import schema
from zope.formlib import form
from zope.interface import implements


class INewsletterSubscriberPortlet(IPortletDataProvider):
    """
    """
    portlet_title = schema.TextLine(
        title=_(u"Title for the portlet."),
        default=u"Newsletter",
        required=True,
    )

    portlet_description = schema.Text(
        title=_(
            u"label_newsletter_description",
            default=u"Description"),
        description=_(
            u"help_newsletter_description",
            default=u"Subscribe here to our newsletter."),
        default=u"",
        required=False)

    newsletter = schema.Choice(
        title=_(
            u"label_newsletter_title",
            default=u"Path to Newsletter"),
        description=_(
            u"help_newsletter_title",
            default=u"The absolute path from portal_root to the newsletter"),
        source=SearchableTextSourceBinder(
            {'portal_type': 'EasyNewsletter'},
            default_query='path:'),
        required=True)

    query_nombre = schema.Bool(
        title=_(
            u"label_newletter_show_nombre",
            default=u"Display field to enter nombre of "
            "subscriber"),
        default=False,
        required=True)

    query_fullname = schema.Bool(
        title=_(
            u"label_newletter_show_fullname",
            default=u"Ask for the salutation and fullname of the subscriber"),
        default=True,
        required=True)

    query_organization = schema.Bool(
        title=_(
            u"label_newletter_show_organization",
            default=u"Display field to enter company/organization of "
            "subscriber"),
        default=False,
        required=True)

    query_movil = schema.Bool(
        title=_(
            u"label_newletter_show_movil",
            default=u"Display field to enter movil of "
            "subscriber"),
        default=False,
        required=True)

    query_pueblo = schema.Bool(
        title=_(
            u"label_newletter_show_pueblo",
            default=u"Display field to enter pueblo of "
            "subscriber"),
        default=False,
        required=True)

    query_pais = schema.Bool(
        title=_(
            u"label_newletter_show_pais",
            default=u"Display field to enter pais of "
            "subscriber"),
        default=False,
        required=True)

    query_sexo = schema.Bool(
        title=_(
            u"label_newletter_show_sexo",
            default=u"Display field to enter sexo of "
            "subscriber"),
        default=False,
        required=True)

    show_unsubscribe = schema.Bool(
        title=_(
            u"label_newletter_show_unsubscribe_link",
            default=u"Display an unsubscribe link in portlet footer"),
        default=True,
        required=True)


class Assignment(base.Assignment):
    """
    """
    implements(INewsletterSubscriberPortlet)

    portlet_title = "Newsletter"
    portlet_description = u""
    newsletter = u""
    query_nombre = False
    query_fullname = True
    query_organization = False
    query_movil = False
    query_pueblo = False
    query_pais = False
    query_sexo = False
    show_unsubscribe = True

    def __init__(
            self, portlet_title=u"", portlet_description=u"", newsletter="",
            query_nombre=False, query_fullname=True, query_organization=False,
            query_movil=False, query_pueblo=False, query_pais=False,
            query_sexo=False, show_unsubscribe=True):

        self.portlet_title = portlet_title
        self.portlet_description = portlet_description
        self.newsletter = newsletter
        self.query_nombre = query_nombre
        self.query_fullname = query_fullname
        self.query_organization = query_organization
        self.query_movil = query_movil
        self.query_pueblo = query_pueblo
        self.query_pais = query_pais
        self.query_sexo = query_sexo
        self.show_unsubscribe = show_unsubscribe

    @property
    def title(self):
        """
        """
        return _(u"Newsletter subscriber portlet")


class Renderer(base.Renderer):
    """
    """
    render = ViewPageTemplateFile('subscriber.pt')

    @property
    def available(self):
        """
        """
        return True

    def header(self):
        return self.data.portlet_title

    def description(self):
        return self.data.portlet_description

    def get_newsletter(self):
        """
        """
        return self.data.newsletter


class AddForm(base.AddForm):
    """
    """
    form_fields = form.Fields(INewsletterSubscriberPortlet)

    def create(self, data):
        """
        """
        return Assignment(
            portlet_title=data.get("portlet_title", u""),
            portlet_description=data.get("portlet_description", u""),
            newsletter=data.get("newsletter", ""),
        )


class EditForm(base.EditForm):
    """
    """
    form_fields = form.Fields(INewsletterSubscriberPortlet)
    label = _(u"Edit Newsletter portlet")
    description = _(
        u"This portlet displays the subscriber add form of a newsletter.")
