# -*- coding: utf-8 -*-
# flake8: noqa
# from zope.i18n import translate
from Products.Archetypes.atapi import DisplayList
from Products.EasyNewsletter import EasyNewsletterMessageFactory as _
import re

PROJECTNAME = "EasyNewsletter"


PLACEHOLDERS = ["UNSUBSCRIBE", "SUBSCRIBER_SALUTATION"]


SALUTATION = DisplayList((
    ('', _(u"label_choose_saluation", "Choose salutation...")),
    ("ms", _(u"label_salutation_ms", "Ms.")),
    ("mr", _(u"label_salutation_mr", "Mr.")),
))

SEXO = DisplayList((
    ('', _(u"label_choose_sexo", "Choose sexo...")),
    ("hombre", _(u"label_sexo_hombre", "Hombre")),
    ("mujer", _(u"label_sexo_mujer", "Mujer")),
))

PAISES = DisplayList((
    ('', _(u"label_pais", "Pais")),
    ("albanian", _(u"Albanian")),
    ("algerian", _(u"Algerian")),
    ("american", _(u"American")),
    ("andorran", _(u"Andorra")),
    ("angolan", _(u"Angolan")),
    ("antiguans", _(u"Antiguans")),
    ("argentinean", _(u"Argentinean")),
    ("armenian", _(u"Armenian")),
    ("australian", _(u"Australian")),
    ("austrian", _(u"Austrian")),
    ("azerbaijani", _(u"Azerbaijani")),
    ("bahamian", _(u"Bahamian")),
    ("bahraini", _(u"Bahraini")),
    ("bangladeshi", _(u"Bangladeshi")),
    ("barbadian", _(u"Barbadian")),
    ("barbudans", _(u"Barbudans")),
    ("batswana", _(u"Batswana")),
    ("belarusian", _(u"Belarusian")),
    ("belgian", _(u"Belgian")),
    ("belizean", _(u"Belizean")),
    ("beninese", _(u"Beninese")),
    ("bhutanese", _(u"Bhutanese")),
    ("bolivian", _(u"Bolivian")),
    ("bosnian", _(u"Bosnian")),
    ("brazilian", _(u"Brazilian")),
    ("british", _(u"British")),
    ("bruneian", _(u"Bruneian")),
    ("bulgarian", _(u"Bulgarian")),
    ("burkinabe", _(u"Burkinabe")),
    ("burmese", _(u"Burmese")),
    ("burundian", _(u"Burundian")),
    ("cambodian", _(u"Cambodian")),
    ("cameroonian", _(u"Cameroonian")),
    ("canadian", _(u"Canadian")),
    ("cape verdean", _(u"Cape Verdean")),
    ("central african", _(u"Central African")),
    ("chadian", _(u"Chadian")),
    ("chilean", _(u"Chilean")),
    ("chinese", _(u"Chinese")),
    ("colombian", _(u"Colombian")),
    ("comoran", _(u"Comoran")),
    ("congolese", _(u"Congolese")),
    ("costa rican", _(u"Costa Rican")),
    ("croatian", _(u"Croatian")),
    ("cuban", _(u"Cuban")),
    ("cypriot", _(u"Cypriot")),
    ("czech", _(u"Czech")),
    ("danish", _(u"Danish")),
    ("djibouti", _(u"Djibouti")),
    ("dominican", _(u"Dominican")),
    ("dutch", _(u"Dutch")),
    ("east timorese", _(u"East Timorese")),
    ("ecuadorean", _(u"Ecuadorean")),
    ("egyptian", _(u"Egyptian")),
    ("emirian", _(u"Emirian")),
    ("equatorial guinean", _(u"Equatorial Guinean")),
    ("eritrean", _(u"Eritrean")),
    ("estonian", _(u"Estonian")),
    ("ethiopian", _(u"Ethiopian")),
    ("fijian", _(u"Fijian")),
    ("filipino", _(u"Filipino")),
    ("finnish", _(u"Finnish")),
    ("french", _(u"French")),
    ("gabonese", _(u"Gabonese")),
    ("gambian", _(u"Gambian")),
    ("georgian", _(u"Georgian")),
    ("german", _(u"German")),
    ("ghanaian", _(u"Ghanaian")),
    ("greek", _(u"Greek")),
    ("grenadian", _(u"Grenadian")),
    ("guatemalan", _(u"Guatemalan")),
    ("guinea-bissauan", _(u"Guinea-Bissauan")),
    ("guinean", _(u"Guinean")),
    ("guyanese", _(u"Guyanese")),
    ("haitian", _(u"Haitian")),
    ("herzegovinian", _(u"Herzegovinian")),
    ("honduran", _(u"Honduran")),
    ("hungarian", _(u"Hungarian")),
    ("icelander", _(u"Icelander")),
    ("indian", _(u"Indian")),
    ("indonesian", _(u"Indonesian")),
    ("iranian", _(u"Iranian")),
    ("iraqi", _(u"Iraqi")),
    ("irish", _(u"Irish")),
    ("israeli", _(u"Israeli")),
    ("italian", _(u"Italian")),
    ("ivorian", _(u"Ivorian")),
    ("jamaican", _(u"Jamaican")),
    ("japanese", _(u"Japanese")),
    ("jordanian", _(u"Jordanian")),
    ("kazakhstani", _(u"Kazakhstani")),
    ("kenyan", _(u"Kenyan")),
    ("kittian and nevisian", _(u"Kittian and Nevisian")),
    ("kuwaiti", _(u"Kuwaiti")),
    ("kyrgyz", _(u"Kyrgyz")),
    ("laotian", _(u"Laotian")),
    ("latvian", _(u"Latvian")),
    ("lebanese", _(u"Lebanese")),
    ("liberian", _(u"Liberian")),
    ("libyan", _(u"Libyan")),
    ("liechtensteiner", _(u"Liechtensteiner")),
    ("lithuanian", _(u"Lithuanian")),
    ("luxembourger", _(u"Luxembourger")),
    ("macedonian", _(u"Macedonian")),
    ("malagasy", _(u"Malagasy")),
    ("malawian", _(u"Malawian")),
    ("malaysian", _(u"Malaysian")),
    ("maldivan", _(u"Maldivan")),
    ("malian", _(u"Malian")),
    ("maltese", _(u"Maltese")),
    ("marshallese", _(u"Marshallese")),
    ("mauritanian", _(u"Mauritanian")),
    ("mauritian", _(u"Mauritian")),
    ("mexican", _(u"Mexican")),
    ("micronesian", _(u"Micronesian")),
    ("moldovan", _(u"Moldovan")),
    ("monacan", _(u"Monacan")),
    ("mongolian", _(u"Mongolian")),
    ("moroccan", _(u"Moroccan")),
    ("mosotho", _(u"Mosotho")),
    ("motswana", _(u"Motswana")),
    ("mozambican", _(u"Mozambican")),
    ("namibian", _(u"Namibian")),
    ("nauruan", _(u"Nauruan")),
    ("nepalese", _(u"Nepalese")),
    ("new zealander", _(u"New Zealander")),
    ("ni-vanuatu", _(u"Ni-Vanuatu")),
    ("nicaraguan", _(u"Nicaraguan")),
    ("nigerien", _(u"Nigerien")),
    ("north korean", _(u"North Korean")),
    ("northern irish", _(u"Northern Irish")),
    ("norwegian", _(u"Norwegian")),
    ("omani", _(u"Omani")),
    ("pakistani", _(u"Pakistani")),
    ("palauan", _(u"Palauan")),
    ("panamanian", _(u"Panamanian")),
    ("papua new guinean", _(u"Papua New Guinean")),
    ("paraguayan", _(u"Paraguayan")),
    ("peruvian", _(u"Peruvian")),
    ("polish", _(u"Polish")),
    ("portuguese", _(u"Portuguese")),
    ("qatari", _(u"Qatari")),
    ("romanian", _(u"Romanian")),
    ("russian", _(u"Russian")),
    ("rwandan", _(u"Rwandan")),
    ("saint lucian", _(u"Saint Lucian")),
    ("salvadoran", _(u"Salvadoran")),
    ("samoan", _(u"Samoan")),
    ("san marinese", _(u"San Marinese")),
    ("sao tomean", _(u"Sao Tomean")),
    ("saudi", _(u"Saudi")),
    ("scottish", _(u"Scottish")),
    ("senegalese", _(u"Senegalese")),
    ("serbian", _(u"Serbian")),
    ("seychellois", _(u"Seychellois")),
    ("sierra leonean", _(u"Sierra Leonean")),
    ("singaporean", _(u"Singaporean")),
    ("slovakian", _(u"Slovakian")),
    ("slovenian", _(u"Slovenian")),
    ("solomon islander", _(u"Solomon Islander")),
    ("somali", _(u"Somali")),
    ("south african", _(u"South African")),
    ("south korean", _(u"South Korean")),
    ("spanish", _(u"Spanish")),
    ("sri lankan", _(u"Sri Lankan")),
    ("sudanese", _(u"Sudanese")),
    ("surinamer", _(u"Surinamer")),
    ("swazi", _(u"Swazi")),
    ("swedish", _(u"Swedish")),
    ("swiss", _(u"Swiss")),
    ("syrian", _(u"Syrian")),
    ("taiwanese", _(u"Taiwanese")),
    ("tajik", _(u"Tajik")),
    ("tanzanian", _(u"Tanzanian")),
    ("thai", _(u"Thai")),
    ("togolese", _(u"Togolese")),
    ("tongan", _(u"Tongan")),
    ("trinidadian or tobagonian", _(u"Trinidadian or Tobagonian")),
    ("tunisian", _(u"Tunisian")),
    ("turkish", _(u"Turkish")),
    ("tuvaluan", _(u"Tuvaluan")),
    ("ugandan", _(u"Ugandan")),
    ("ukrainian", _(u"Ukrainian")),
    ("uruguayan", _(u"Uruguayan")),
    ("uzbekistani", _(u"Uzbekistani")),
    ("venezuelan", _(u"Venezuelan")),
    ("vietnamese", _(u"Vietnamese")),
    ("welsh", _(u"Welsh")),
    ("yemenite", _(u"Yemenite")),
    ("zambian", _(u"Zambian")),
    ("zimbabwean", _(u"Zimbabwean")),
    ("Otro", _(u"Altre")),
))


MESSAGE_CODE = {
    "email_added": _(
        u"email_added",
        default=u"Your email has been registered. A confirmation email was"
                u" sent to your address. Please check your inbox and click "
                u" on the link in the email in order to confirm your"
                u" subscription."
    ),
    "invalid_email": _(
        u"invalid_email", default=u"Please enter a valid email address."),
    "email_exists": _(
        u"email_exists", default=u"Your email address is already registered."),
    "invalid_hashkey": _(
        u"invalid_hashkey", default=u"Please enter a valid email address."),
    "subscription_confirmed": _(
        u"subscription_confirmed",
        default=u"Your subscription was successfully confirmed."),
    }


EMAIL_RE = re.compile(
    r"(?:^|\s)[-a-z0-9_.]+@(?:[-a-z0-9]+\.)+[a-z]{2,6}(?:\s|$)", re.IGNORECASE)


DEFAULT_TEMPLATE = """\
<table border="0" cellpadding="10" cellspacing="10" width="100%">
<tal:block tal:repeat="object context/queryCatalog"
    i18n:domain="EasyNewsletter">
  <tr>
    <td>
      <h2 class="tileHeadline"><a tal:attributes="href object/getURL"
        tal:content="object/Title">Title</a></h2>
      <p class="tileBody">
        <span tal:content="object/Description">Description</span>
      </p>
      <p class="tileFooter">
        <a tal:attributes="href object/getURL"
            i18n:translate="read_more">Read more</a>&hellip;
      </p>
    </td>
    <td width="164" align="right">
      <tal:image_obj tal:define="item_object object/getObject;">
        <tal:block
            condition="python:object.portal_type in ['Image', 'News Item']">
          <a tal:attributes="href object/getURL">
            <img class="tileImage"
                tal:condition="python:hasattr(item_object, 'tag')"
tal:attributes="src python:object.getURL(relative=1) + '/@@images/image/thumb'"
                />
          </a>
        </tal:block>
      </tal:image_obj>
    </td>
  </tr>
</tal:block>
</table>

<tal:block tal:repeat="subtopic context/getSubTopics">
<table border="0" cellpadding="10" cellspacing="10" width="100%">
  <tr>
    <th>
      <h1 tal:content="subtopic/Title">Title</h1>
    </th>
  </tr>
<tal:blockitems tal:repeat="object subtopic/queryCatalog"
    i18n:domain="EasyNewsletter">
  <tr>
    <td>
      <h2 class="tileHeadline"><a
        tal:attributes="href object/getURL"
        tal:content="object/Title">Title</a></h2>
      <p class="tileBody">
        <span tal:content="object/Description">Description</span>
      </p>
      <p class="tileFooter">
        <a tal:attributes="href object/getURL"
            i18n:translate="read_more">Read more</a>&hellip;
      </p>
    </td>
  </tr>
  <tr>
    <td width="164" align="right">
      <tal:image_obj tal:define="item_object object/getObject;">
        <tal:block
            condition="python:object.portal_type in ['Image', 'News Item']">
        <a tal:attributes="href object/getURL">
          <img class="tileImage"
              tal:condition="python:hasattr(item_object, 'tag')"
tal:attributes="src python:object.getURL(relative=1) + '/@@images/image/thumb'"
              />
        </a>
        </tal:block>
      </tal:image_obj>
    </td>
  </tr>
</tal:blockitems>
</tal:block>"""


DEFAULT_OUT_TEMPLATE_PT = """<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><tal:title content="context/Title" /></title>
<style type="text/css">
body {
  color: #333 !important;
}
h1, h2, h3, h1 a, h2 a, h3 a {
  color: #979799! important;
}
th, td {
  padding: 0;
}
tileItem {
  display: block;
}
img.tileImage {
  float: right;
  margin: 10px 0 10px 10px !important;
}
.visualClear {
  clear: both;
  display: block;
}
</style>
</head>
<body>
  <table width="100%" cellpadding="0" cellspacing="0">
    <tr>
      <td>
        <div class="mailonly"
            tal:define="portal_url context/@@plone_portal_state/portal_url">
          <img src="logo.png" />
        </div>
      </td>
    </tr>
    <tr>
      <td height="10px">
        <!-- -->
      </td>
    </tr>
    <tr>
      <td height="1"
        style="background-color: #284d7b; height: 1px;"><!-- --></td>
    </tr>
    <tr>
      <td height="20">
        <!-- -->
      </td>
    </tr>
    <tr>
      <td class="newsletter_link">
        <div class="mailonly">
          <p><a
            tal:attributes="href context/absolute_url"
                >View Newsletter in Web-browser</a></p>
        </div>
      </td>
    </tr>
    <tr>
      <td class="header">
        <!-- this is the header of the newsletter -->
        <span tal:replace="structure context/getHeader" />
        <span
        tal:define="toLocalizedTime nocall:context/@@plone/toLocalizedTime"
tal:replace="structure python:toLocalizedTime(context.modified(), long_format=0)" />
      </td>
    </tr>
    <tr>
      <td class="body">
        <!-- this is the main text of the newsletter -->
        <div class="mailonly">
          <p><span tal:replace="structure context/Description" /></p>
        </div>
        <span tal:replace="structure context/getText" />
        <tal:def tal:define="files context/getFiles">
            <dl id="file-attachments" tal:condition="files">
                <tal:loop repeat="file files">
                    <dt>
                        <a tal:attributes="href file/getURL"
                           tal:content="file/Title" />
                    </dt>
                    <dd tal:content="file/Description" />
                </tal:loop>
            </dl>
        </tal:def>
      </td>
    </tr>
    <tr>
      <td class="footer">
        <!-- this is the footer of the newsletter -->
        <span tal:replace="structure context/getFooter" />
      </td>
    </tr>
  </table>
</body>
</html>"""


DEFAULT_SUBSCRIBER_CONFIRMATION_MAIL_SUBJECT = _(
    u"Confirm your subscription on ${portal_url}"
)

DEFAULT_SUBSCRIBER_CONFIRMATION_MAIL_TEXT = """\
You subscribe to the ${newsletter_title} Newsletter.\n\n
Your registered email is: ${subscriber_email}\n
Please click on the link to confirm your subscription: \n
${confirmation_url}"""
